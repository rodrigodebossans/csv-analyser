# coding=utf-8

from app.dao.base import Base
from app.service.csv.csv_service import CsvService
from app.service.log.log_service import LogService

import logging


class App:

    def __init__(self):
        self.base = Base.get_instance().get_base()
        self.engine = Base.get_instance().get_engine()
        self.session = Base.get_instance().get_session()
        self.base.metadata.create_all(self.engine)

    def start(self):
        logging.info('Script iniciado\n')
        logErrors = {}
        try:
            session = self.session()
            CsvService.insert_data(session)
            CsvService.generate_three_data(session)
            CsvService.export_three_data(session)
        except Exception as err:
            logErrors[err.__class__.__name__] = err.__str__()
            session.rollback()
            logging.error(logErrors.__str__())
            print(logErrors.__str__())
            LogService.insert_log(session, logErrors.__str__())
        else:
            logging.info('Inserçao de dados iniciada...')
            LogService.insert_log(session, 'processado com sucesso!', True)
            logging.info('...Processado com sucesso!\n')
        finally:
            session.commit()
            session.close()
            logging.info('Script finalizado')


if __name__ == '__main__':
    app = App()
    app.start()
