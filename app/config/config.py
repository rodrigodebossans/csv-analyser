# coding=utf-8

from dotenv import load_dotenv

import logging


class Config:

    def __init__(self):
        load_dotenv()
        logging.basicConfig(
            filename='.log',
            filemode='w',
            level=logging.NOTSET,
            format='%(levelname)s: (%(asctime)s) -> %(message)s',
            datefmt='%d/%m/%Y %H:%M:%S'
        )
