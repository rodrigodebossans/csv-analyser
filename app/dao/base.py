# coding=utf-8

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from app.config.config import Config

import os


class Base(Config):

    _instance = None

    def __init__(self):
        super(Base, self).__init__()
        self.engine = create_engine(
            '{0}+{1}://{2}:{3}@{4}/{5}'
            .format(
                os.getenv('db.database'),
                os.getenv('db.driver'),
                os.getenv('db.user'),
                os.getenv('db.pwd'),
                os.getenv('db.host'),
                os.getenv('db.db')
            )
        )
        self.session = sessionmaker(bind=self.engine)
        self.base = declarative_base()

    def get_engine(self):
        return self.engine

    def get_session(self):
        return self.session

    def get_base(self):
        return self.base

    @classmethod
    def get_instance(cls):
        if cls._instance is None:
            cls._instance = cls()
        return cls._instance
