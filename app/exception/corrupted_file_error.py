# coding=utf-8

from app.exception.error import Error


class CorruptedFileError(Error):

    def __init__(self, message=None):
        self.message = message
        super(CorruptedFileError, self).__init__(self.message)

    def __str__(self):
        return self.message
