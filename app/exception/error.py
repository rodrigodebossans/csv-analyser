# coding=utf-8


class Error(Exception):

    def __init__(self, message):
        self.message = message
        super(Exception, self).__init__(self.message)
