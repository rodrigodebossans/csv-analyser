# coding=utf-8

from app.exception.error import Error


class RequiredFieldError(Error):

    def __init__(self, message=None):
        self.message = message
        super(RequiredFieldError, self).__init__(self.message)

    def __str__(self):
        return self.message
