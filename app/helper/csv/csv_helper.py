# coding=utf-8

from app.exception.corrupted_file_error import CorruptedFileError

import csv


class CsvHelper:

    @staticmethod
    def get_csv(path):
        try:
            with open(path, 'r') as dataCsvOne:
                ret = []
                for line in csv.DictReader(dataCsvOne, delimiter=';'):
                    ret.append(line)
                return ret
        except Exception:
            raise CorruptedFileError('Erro na leitura do CSV => {}'.format(path))
