# coding=utf-8

from sqlalchemy import Column, Integer, DateTime, String
from app.exception.required_field_error import RequiredFieldError
from app.dao.base import Base


class DataCsvOne(Base.get_instance().get_base()):
    __tablename__ = 'data_csv_one'

    id = Column('ID', Integer, nullable=False, primary_key=True)
    start_date = Column('DATA_INICIO', DateTime, nullable=False)
    name = Column('NOME', String(255), nullable=False)
    note = Column('NOTA', String(255), nullable=False)
    unity = Column('UNIDADE', Integer, nullable=False)

    def __init__(self, id, start_date, name, note, unity):
        self.id = id
        self.start_date = start_date
        self.name = name
        self.note = note
        self.unity = unity

    def validate(self):
        if (not self.id) or (not self.name) or (not self.name) or (not self.note) or (not self.unity):
            raise RequiredFieldError('Preencha e verifique todos os campos do primeiro CSV')
        else:
            return self

