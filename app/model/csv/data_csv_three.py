# coding=utf-8

from sqlalchemy import Column, Integer, String, DateTime, Float
from app.dao.base import Base


class DataCsvThree(Base.get_instance().get_base()):
    __tablename__ = 'data_csv_three'

    id = Column('ID', Integer, nullable=False, primary_key=True)
    start_date = Column('DATA_INICIO', DateTime)
    name = Column('NOME', String(255), nullable=False)
    note = Column('NOTA', String(255), nullable=False)
    unity = Column('UNIDADE', Integer, nullable=False)
    end_date = Column('DATA_FINAL', DateTime, nullable=False)
    value = Column('VALOR', Float, nullable=False)

    def __init__(self, start_date, name, note, unity, end_date, value):
        self.start_date = start_date
        self.name = name
        self.note = note
        self.unity = unity
        self.end_date = end_date
        self.value = value

