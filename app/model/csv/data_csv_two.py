# coding=utf-8

from sqlalchemy import Column, Integer, DateTime, Float
from app.dao.base import Base
from app.exception.required_field_error import RequiredFieldError


class DataCsvTwo(Base.get_instance().get_base()):
    __tablename__ = 'data_csv_two'

    id = Column('ID', Integer, nullable=False, primary_key=True)
    end_date = Column('DATA_FINAL', DateTime, nullable=False)
    value = Column('VALOR', Float, nullable=False)

    def __init__(self, id, end_date, value):
        self.id = id
        self.end_date = end_date
        self.value = value

    def validate(self):
        if (not self.id) or (not self.end_date) or (not self.value):
            raise RequiredFieldError('Preencha e verifique todos os campos do segundo CSV')
        else:
            return self
