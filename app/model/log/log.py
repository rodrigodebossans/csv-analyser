# coding=utf-8

from datetime import date as Date
from sqlalchemy import Column, Integer, DateTime, Boolean, Text
from app.dao.base import Base


class Log(Base.get_instance().get_base()):
    __tablename__ = 'log'

    id = Column('ID', Integer, nullable=False, primary_key=True)
    date = Column('DATA', DateTime, nullable=False)
    description = Column('DESCRICAO', Text(4294000000), nullable=False)
    success = Column('SUCESSO', Boolean, nullable=False)

    def __init__(self, description, success=False, date=Date.today()):
        self.date = date
        self.description = description
        self.success = success
