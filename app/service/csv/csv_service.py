# coding=utf-8

from datetime import datetime
from app.helper.csv.csv_helper import CsvHelper
from app.model.csv.data_csv_one import DataCsvOne
from app.model.csv.data_csv_two import DataCsvTwo
from app.model.csv.data_csv_three import DataCsvThree
from app.config.config import Config

import os
import csv
import logging


class CsvService(Config):

    def __init__(self):
        super(CsvService, self).__init__()

    @staticmethod
    def insert_data(session):
        logging.info('Carregamento de dados iniciado...')
        try:

            for line in CsvHelper.get_csv(os.getenv('path.csv.one')):
                date = datetime.strptime(line['DATA_INICIO'], '%d/%m/%Y') if line['DATA_INICIO'] else None
                unity = int(line['UNIDADE']) if line['UNIDADE'] else None
                session.add(DataCsvOne(line['ID'], date, line['NOME'], line['NOTA'], unity).validate())

            for line in CsvHelper.get_csv(os.getenv('path.csv.two')):
                date = datetime.strptime(line['DATA_FIM'], '%d/%m/%Y %H:%M') if line['DATA_FIM'] else None
                value = float(line['VALOR'].replace(',', '.')) if line['VALOR'] else None
                session.add(DataCsvTwo(line['ID'], date, value).validate())

        except Exception as err:
            logging.error('Erro => {}'.format(err.__str__()))
            raise err
        else:
            logging.info('...Processado com sucesso!')

    @staticmethod
    def generate_three_data(session):
        logging.info('Geraçao de dados iniciada...')
        for line in session.query(DataCsvOne, DataCsvTwo).filter(DataCsvOne.id == DataCsvTwo.id).all():
            session.add(
                DataCsvThree(
                    line[0].start_date,
                    line[0].name,
                    line[0].note,
                    line[0].unity,
                    line[1].end_date,
                    line[1].value
                )
            )
        logging.info('...Processado com sucesso!')

    @staticmethod
    def export_three_data(session):
        try:
            logging.info('Exportaçao de dados iniciada...')
            data_to_csv = session.query(DataCsvThree).all()

            file_name = os.getenv('name.output.csv')
            path_dir = os.getenv('path.output.csv')
            path_file = os.path.join(path_dir, file_name)

            if not os.path.exists(path_dir):
                os.makedirs(path_dir)
                logging.info('Diretorio criado: {}'.format(path_dir))

            with open(path_file, 'w') as three_csv:
                columns = ['ID', 'DATA_INICIO', 'NOME', 'NOTA', 'UNIDADE', 'DATA_FIM', 'VALOR']
                writer = csv.DictWriter(
                    three_csv,
                    fieldnames=columns,
                    delimiter=';',
                    lineterminator='\n'
                )
                writer.writeheader()
                for line in data_to_csv:
                    writer.writerow({
                        'ID': line.id,
                        'DATA_INICIO': line.start_date,
                        'NOME': line.name,
                        'NOTA': line.note,
                        'UNIDADE': line.unity,
                        'DATA_FIM': line.end_date,
                        'VALOR': line.value
                    })
        except Exception as err:
            raise err
        else:
            logging.info('...Processado com sucesso!')
