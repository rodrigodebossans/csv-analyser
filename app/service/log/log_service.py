# coding=utf-8

from app.model.log.log import Log


class LogService:

    @staticmethod
    def insert_log(session, description, success=False):
        session.add(Log(description, success))
